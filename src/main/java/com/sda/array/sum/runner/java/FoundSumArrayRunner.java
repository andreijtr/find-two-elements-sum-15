package com.sda.array.sum.runner.java;

import com.sda.array.sum.login.java.FoundSumArray;

public class FoundSumArrayRunner {

    public static void main(String[] args) {

        int[] array = {1, 3, 5, 8, 11, 12, 18, 19, 21, 32, 45};
        FoundSumArray findSum = new FoundSumArray(array,77);

        findSum.foundElements();
    }
}
