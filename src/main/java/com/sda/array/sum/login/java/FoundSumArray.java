package com.sda.array.sum.login.java;

import java.util.HashMap;

public class FoundSumArray {
    
    private int[] sortArray;
    private int sumToFind;
    
    public FoundSumArray(int[] sortArray, int sumToFind) {
        this.sortArray = sortArray;
        this.sumToFind = sumToFind;
    }

    public void foundElements() {

        int i = 0;
        int j;
        boolean isFound = false;

        while (i < this.sortArray.length && !isFound) {
            j = 0;
            while (j <= i && (i + j) <= this.sumToFind) {
                if((this.sortArray[i] + this.sortArray[j]) == this.sumToFind) {
                    System.out.println("Index " + i + " : " + this.sortArray[i]);
                    System.out.println("Index " + j + " : " + this.sortArray[j]);
                    isFound = true;
                }
                j++;
            }
            i++;
        }
    }
    
}
